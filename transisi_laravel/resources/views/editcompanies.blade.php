@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-6 card">
        <div class="text-center">
            <h1>Edit Data Companies</h1><br>
        </div>
       
        <form method="POST" action="{{ route('home.update', $companies->id) }}" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
              <label for="formGroupExampleInput">Nama</label>
              <input type="text" name="nama" class="form-control" id="formGroupExampleInput" value="{{ $companies->nama }}">
              @error('nama')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2">Email</label>
              <input type="email" name="email" class="form-control" id="formGroupExampleInput2" value="{{ $companies->email }}">
              @error('email')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2">Alamat Website</label>
              <input type="text" name="website" class="form-control" id="formGroupExampleInput2" placeholder="Masukan Alamat Website" value="{{ $companies->website }}">
              @error('website')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <img width="100" height="100" src="{{ asset('storage/app/company/' . $companies->logo) }}" alt="">
            <div class="custom-file mt-2">
                <input type="file" name="logo" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Abaikan Jika Tidak ingin Mengubah Logo</label>
                @error('logo')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
              </div>

              <div class="text-right">
                  <a class="btn btn-success mt-2" href="{{ route('home.index') }}">Kembali</a>
                  <button type="submit" class="btn btn-primary mt-4 mb-3 mr-2" >Simpan</button>
              </div>
          </form>
    </div>
</div>
    
@endsection