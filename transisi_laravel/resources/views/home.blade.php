@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group list-group-horizontal justify-content-center">
                <li class="list-group-item"><a href="{{ route('home.index') }}">Company</a></li>
                <li class="list-group-item"><a href="{{ route('employees.index') }}">Employe</a></li>
            </ul>

            <a class="btn btn-primary mb-3 mt-2" href="{{ route('home.create') }}">Tambah Companies</a>
            <div class="card">
                <div class="card-header"><h3>Data Companie</h3></div>
                <div class="card-body">
                    <div class="row">
                        
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Website</th>
                                    <th scope="col">Logo</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            @forelse ($companies as $row)
                            <tbody>
                              <tr>
                                <th scope="row">{{ $row->nama }}</th>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->website }}</td>
                                <td><img src="{{ asset('storage/app/company/'. $row->logo) }}"></td>
                                <td>
                                    <form method="POST" action="{{ route('home.destroy' , $row->id) }}">
                                        @csrf
                                        @method('delete')
                                        
                                        <a class="btn btn-warning" href="{{ route('home.edit', $row->id) }}">Edit</a>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                      </form>
                                </td>
                              </tr>
                        @empty
                        <td colspan="5">Data Masih Kosong</td>
                        @endforelse
                        </tbody>
                    </table>

                    {{ $companies->links() }}
                        
                        
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
