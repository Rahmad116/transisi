@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group list-group-horizontal justify-content-center">
                <li class="list-group-item"><a href="{{ route('home.index') }}">Company</a></li>
                <li class="list-group-item"><a href="{{ route('employees.index') }}">Employe</a></li>
            </ul>
    
            <a class="btn btn-primary mb-3 mt-2" href="{{ route('employees.create') }}">Tambah Employe</a>
            <div class="card">
                <div class="card-header"><h3>Data Employees</h3></div>
                <div class="card-body">
                    <div class="row">
                        
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Company</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            @forelse ($employe as $row)
                            <tbody>
                              <tr>
                                <th scope="row">{{ $row->nama }}</th>
                                <td>{{ $row->companies->nama }}</td>
                                <td>{{ $row->email }}</td>
                                <td>
                                    <form method="POST" action="{{ route('employees.destroy' , $row->id) }}">
                                        @csrf
                                        @method('delete')
                                        
                                        <a class="btn btn-warning" href="{{ route('employees.edit', $row->id) }}">Edit</a>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                      </form>
                                </td>
                              </tr>
                        @empty
                        <td colspan="5">Data Masih Kosong</td>
                        @endforelse
                        </tbody>
                    </table>
    
                    {{ $employe->links() }}
                        
                        
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection