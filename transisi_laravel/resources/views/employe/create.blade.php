@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-6 card">
        <div class="text-center">
            <h1>Tambah Data Companies</h1><br>
        </div>
       
        <form method="POST" action="{{ route('employees.store') }}">
            @csrf
            <div class="form-group">
              <label for="formGroupExampleInput">Nama</label>
              <input type="text" name="nama" class="form-control" id="formGroupExampleInput" placeholder="Masukan Nama Employees" value="{{ old('nama') }}">
              @error('nama')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
                <label for="companies_id">Companies</label>
                <select name="companies_id" class="form-control">
                    <option value="">Pilih</option>
                    @foreach ($companies as $row)
                    <option value="{{ $row->id }}" {{ old('companies_id') == $row->id ? 'selected':'' }}>{{ $row->nama }}</option>
                    @endforeach
                </select>
                @error('companies_id')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2">Email</label>
              <input type="email" name="email" class="form-control" id="formGroupExampleInput2" placeholder="Masukan Email Employees" value="{{ old('email') }}">
              @error('email')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>

              <div class="text-right">
                <a class="btn btn-success mt-2" href="{{ route('employees.index') }}">Kembali</a>
                  <button type="submit" class="btn btn-primary mt-4 mb-3 mr-2" >Simpan</button>
              </div>
          </form>
    </div>
</div>
    
@endsection