<?php

namespace App\Http\Controllers;

use App\Companies;
use Illuminate\Http\Request;
use File;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Companies::paginate(5);

        return view('home', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addcompany');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'logo' => 'required|mimes:png|max:2048|dimensions:max_width=100,max_height=100',
            'website' => 'required',
        ]);

        if($request->has('logo'))
        {
            $logo = $request->logo;
            $new_logo = time() . $logo->getClientOriginalName();
            $logo->storeAs('public/app/company', $new_logo);

            $companies = Companies::create([
                'nama' => $request->nama,
                'email' => $request->email,
                'logo' => $new_logo,
                'website' => $request->website,
            ]);
            return redirect(route('home.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function show(Companies $companies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = Companies::find($id);
        return view('editcompanies', compact('companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'logo' => 'nullable|mimes:png|max:2048|dimensions:max_width=100,max_height=100',
            'website' => 'required',
        ]);

        $companies = Companies::find($id);
        $nama_logo = $companies->logo;

        if($request->hasFile('logo'))
        {
            $logo = $request->logo;
            $nama_logo = time() . $logo->getClientOriginalName();
            $logo->storeAs('public/app/company', $nama_logo);
            File::delete(storage_path('app/public/app/company/' . $companies->logo));
        }

        $companies->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'logo' => $nama_logo,
            'website' => $request->website,
        ]);
        return redirect(route('home.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $companies = Companies::find($id);

        File::delete(storage_path('app/public/app/company/' . $companies->logo));
        $companies->delete();

        return redirect(route('home.index'));
    }
}
