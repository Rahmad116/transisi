<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = 'employees';
    protected $guarded = ['id'];

    public function companies()
    {
        return $this->belongsTo(Companies::class);
    }
}
