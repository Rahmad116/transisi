<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                    <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="#">Features</a>
                    <a class="nav-item nav-link" href="#">Pricing</a>
                    <a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </div>
                </div>
                </nav>

                <div class="dasar_1">
                    <h5>PHP DASAR (1)</h5>
                    <?php 
                        $nilai = "72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86";
                        echo "Daftar Nilai Siswa : ". $nilai;
                        echo('<br>');
                        $angka = explode(" ", $nilai);

                        $jumlah_siswa = count($angka);
                        $jumlah_nilai = array_sum($angka);
                        $rata2 = $jumlah_nilai/$jumlah_siswa;
                        echo "nilai rata-rata " . $rata2;
                    ?>
                    
                    <br>
                    <?php
                        arsort($angka);
                        $tertinggi = array_slice($angka, 0, 7);
                        echo('7 nilai teratas : ');
                        foreach ($tertinggi as $row)
                        {
                            echo $row;
                            echo(', ');
                        }
                    ?>

                    <br>
                    <?php
                        sort($angka);
                        $terendah = array_slice($angka, 0,7);
                        echo('7 nilai terendah : ');
                        foreach ($terendah as $row)
                        {
                            echo $row;
                            echo (', ');
                        }
                    ?>
                        <br>
                    <?php
                        // function CountCaps($string) {
                        //  return strlen(preg_replace('/[^A-Z]+/', '', $string));
                        // }
                        function hitungHurufKecil($string) 
                        {
                            return strlen(preg_replace('/[^a-z]+/', '', $string));
                        }
                        $kalimat = "TranSISI";
                        $jml_kecil = hitungHurufKecil($kalimat);
                        echo "Inputan User : ". $kalimat;
                        echo '<br>';
                        echo $kalimat . ' Mengandung ' .$jml_kecil. ' huruf kecil';
                    ?>

                         <br>

                    <?php
                        $kalimat2 = "Jakarta adalah ibukota negara Republik Indonesia";

                        // $jumlah_kata = str_word_count($kalimat2);
                        // print_r($jumlah_kata);
                        $row = explode(' ', $kalimat2);
                        echo 'Unigram : ';
                        foreach($row as $r)
                        {
                            echo $r;
                            echo ', ';
                        }

                        echo ('<br>');

                        $kalimat3 = $row[0] . ' '.  $row[1];
                        $kalimat4 = $row[2] . ' '.  $row[3];
                        $kalimat5 = $row[4] . ' '.  $row[5];
                        
                        $bigram = $kalimat3 . ', ' . $kalimat4 . ', ' . $kalimat5;
                        echo "Bigram : ". $bigram;

                        echo ('<br>');

                        $kalimat6 = $row[0] . ' ' .  $row[1] . ' ' .  $row[2];
                        $kalimat7 = $row[3] . ' ' .  $row[4] . ' ' .  $row[5];
                        $trigram = $kalimat6 . ', '. $kalimat7;

                        echo "Trigram : ". $trigram;

                    ?>
                </div>

                <div class="dasar2">
                             <h5 class="mt-3">PHP DASAR (2)</h5>
                
                        <?php
                            function BuatTabel($baris, $kolom)
                            {
                                    $color = '#CCC';
                                    $angka = range(1, 64);
                                    $k = 0;
                                
                                print("<table align=\"center\" width = \"50%\" border=\"1\">\n");
                            
                                    for($b = 0; $b < $baris; $b++) 
                                    {
                                        $color = ($color == '#CCC') ? '#2d3436' : '#CCC';
                                        print("<tr bgcolor='$color' \n ");
                                            for($k = 0; $k < $kolom; $k++) 
                                            {
                                                print("<td height=\"40\" width=\"40\"></td>\n");         
                                            }
                                        print("</tr>\n");
                                    }
                                         print("</table>\n");
                            }
                        ?>

                        <div class="col-md-8">
                            <?php
                            echo BuatTabel(8,9);
                            ?>
                        </div>

                            <br>

                        <?php
                        $input = 'DFHKNQ';
                        function enkripsi($b)
                            {
                            $awalnya = ["D","F","H","K","N","Q"];
                            $gantinya =   ["E","D","K","G","S","K"];
                            $hasilnya = str_replace($awalnya, $gantinya, $b);
                            echo $hasilnya;
                            }

                            echo enkripsi($input);
                        ?>
                </div>


                <div class="col-md-8 mt-4 mb-5">
                        <h5>PHP DASAR (3)</h5>
                        <?php              

                            function cari(Array $array, String $keywords) 
                            {
                                $keywords = str_split($keywords);
                                $arrayMerge = [];
                                
                                foreach($array as $index => $value):
                                    $arrayMerge = array_merge($arrayMerge, $value);
                                endforeach;	
                                
                                
                                foreach($keywords as $index => $keyword):
                                    if(!in_array($keyword, $arrayMerge)):
                                        return false;
                                        exit();
                                    endif;
                                endforeach;
                                
                                return true;
                            }

                            $arr = [
                                ['f', 'g', 'h', 'i'],
                                ['j', 'k', 'p', 'q'],
                                ['r', 's', 't', 'u']
                            ];

                            // inputan user
                            $cari = 'fghp';

                            var_dump (cari($arr, $cari));
                        ?>
                </div>

                
            </div>
        </div>
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>